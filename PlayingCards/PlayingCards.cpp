
// Playing Cards 
// Part 1: Mason Brull
// Part 2: Seth Glisczinski

#include <iostream>
#include <conio.h>

using namespace std;

enum Rank
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	SPADE,
	HEART,
	CLUB,
	DIAMOND
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card);
Card HighCard(Card card1, Card card2);

int main()
{
	Card card;
	card.rank = SEVEN;
	card.suit = CLUB;
	
	PrintCard(card);

	Card card1;
	card1.rank = JACK;
	card1.suit = SPADE;

	Card card2;
	card2.rank = KING;
	card2.suit = DIAMOND;

	HighCard(card1, card2); // Does not print to console
	PrintCard(HighCard(card1, card2)); // Prints out the HighCard, showing that it hold tha value of a card

	(void)_getch();
	return 0;
}

void PrintCard(Card card)
{
	

	switch (card.rank)
	{
		case ACE: cout << "The Ace "; break;
		case KING: cout << "The King "; break;
		case QUEEN: cout << "The Queen "; break;
		case JACK: cout << "The Jack "; break;
		case TEN: cout << "The Ten "; break;
		case NINE: cout << "The Nine "; break;
		case EIGHT: cout << "The Eight "; break;
		case SEVEN: cout << "The Seven "; break;
		case SIX: cout << "The Six "; break;
		case FIVE: cout << "The Five "; break;
		case FOUR: cout << "The Four "; break;
		case THREE: cout << "The Three "; break;
		case TWO: cout << "The Two ";
	}

	switch (card.suit)
	{
		case SPADE: cout << "of Spades.\n"; break;
		case CLUB: cout << "of Clubs.\n"; break;
		case HEART: cout << "of Hearts.\n"; break;
		case DIAMOND: cout << "of Diamonds.\n";
	}
}

Card HighCard(Card card1, Card card2)
{
	
	if (card1.rank >= card2.rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}
